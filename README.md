## Prerequisites

* Golang 1.16.x
* Docker 19.03+
* Docker Compose 1.25+

## Installation

```sh
docker-compose up -d 
```

# Running test

```
$ go test .\service 
```

## Used Technologies

* Golang 1.16.3
* Echo
